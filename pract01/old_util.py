import csv
import networkx as nx
import math
import matplotlib.pyplot as plt


def get_subway_graph(csv_dir, Klass):
    is_personal_graph_class = True
    if str(Klass) == "<class 'networkx.classes.graph.Graph'>":
        is_personal_graph_class = False
    G = Klass()
    # "line","name","colour","stripe"
    lines = {}
    with open(csv_dir+'/lines.csv', 'r') as csvfile:
        creader = csv.reader(csvfile)
        next(creader, None)
        for row in creader:
            lines[int(row[0])] = {"name": row[1], "color": row[2], "stripe": row[3], "line":int(row[0])}

    with open(csv_dir+'/connections.csv', 'r') as csvfile:
        creader = csv.reader(csvfile)
        next(creader, None)

        if is_personal_graph_class:
            for row in creader:
                G.add_edge(int(row[0]), int(row[1]), attr_dict=lines[int(row[2])])
        else:
            for row in creader:
                G.add_edge(int(row[0]), int(row[1]),   name=lines[int(row[2])]['name'],
                                                       color=lines[int(row[2])]['color'],
                                                       stripe=lines[int(row[2])]['stripe'],
                                                       line=lines[int(row[2])]['line'])

    with open(csv_dir+'/stations.csv', 'r') as csvfile:
        creader = csv.reader(csvfile)
        next(creader, None)
        if is_personal_graph_class:
            for row in creader:
                G.node[int(row[0])] = {"latitude": float(row[1]),
                                       "longitude": float(row[2]),
                                       "name": row[3],
                                       "display_name": row[4],
                                       "zone": float(row[5]),
                                       "total_lines": int(row[6]),
                                       "rail": row[7]
                                      }
        else:
            for row in creader:
                G.add_node(int(row[0]),    latitude=float(row[1]),
                                           longitude=float(row[2]),
                                           name=row[3],
                                           display_name=row[4],
                                           zone=float(row[5]),
                                           total_lines=int(row[6]),
                                           rail=row[7])

    for node1, node2 in G.edges():
        norm = math.sqrt(
                (G.node[node1]['longitude'] - G.node[node2]['longitude'])**2 +
                (G.node[node1]['latitude'] - G.node[node2]['latitude'])**2
        )
        if is_personal_graph_class:
            G.edge[node1][node2].update({'distance': norm})
        else:
            G.add_edge(node1, node2, distance=norm)

    return G, lines

def draw_subway_graph(G, lines, figsize=(10,6), show_labels=False):
    plt.figure(figsize=figsize)
    plt.axis('off')
    G2 = graph2nx(G)
    pos = {x: (G2.node[x]['longitude'], G2.node[x]['latitude']) for x in G2.node.keys()}
    nx.draw_networkx_nodes(G2, 
                           pos, 
                           node_size=1,
                          )
    if show_labels:
        nx.draw_networkx_labels(G2,pos,
                                {x: G2.node[x]['name'] for x in G2.nodes()},font_size=4)

    for line in lines.keys():
        nx.draw_networkx_edges(
            G2,
            pos,
            edgelist=[x for x in G2.edges() if G.edge[x[0]][x[1]]['line'] == line],
            edge_color="#"+lines[line]['color'],
        )

    plt.show()


def graph2nx(gr):
    G = nx.Graph()
    for node1 in gr.edge.keys():
        for node2, value in gr.edge[node1].items():
            G.add_edge(node1, node2, **value)

    for node, value in gr.node.items():
        G.add_node(node, **value)
       
    return G

def dijkstra_tester(graph_class,dijkstra_method,origen=10,destino=235):
    # Graph loading using NetworkX Graph library
    nxG, lines_nxG = get_subway_graph('csv', nx.graph.Graph)
    # Graph loading using my Graph library
    G, lines_G = get_subway_graph('csv', graph_class)
    # Graph in NetworkX format converting from my graph
    G2nx = graph2nx(G)

    # Get shortest path for all 3 graphs
    real_path = nx.dijkstra_path(nxG, origen, destino, 'distance')
    my_path = dijkstra_method(G, origen, destino)
    my_maybe_path = nx.dijkstra_path(G2nx, origen, destino, 'distance')
    

    real_distance = 0
    for i in range(len(real_path) - 1):
        real_distance += (nxG[real_path[i]][real_path[i + 1]]['distance'])

    my_maybe_distance = 0
    for i in range(len(my_maybe_path) - 1):
        my_maybe_distance += (G[my_maybe_path[i]][my_maybe_path[i + 1]]['distance'])

    print("#####   DISTANCES   #####")
    print(("Real  : %.10f" % real_distance))
    print(("Mine  : %.10f" % my_path['distance']))
    print(("Mine? : %.10f" % my_maybe_distance))

    print("\n#####   PATHS   #####")
    print("Real : " + str(real_path))
    print("Mine : " + str(my_path['path']))
    print("Mine?: " + str(my_maybe_path))

    if my_path['path'] == real_path:
        print("\nTodo correcto!")
    elif my_path['path'] == my_maybe_path and my_path['path'] != real_path:
        print("\nLibreria de grafos incorrecta")
    else:
        print("\nDijkstra incorrecto")
