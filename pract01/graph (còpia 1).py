from collections import defaultdict

nested_dict = lambda: defaultdict(nested_dict)

class Graph:

    def __init__(self):
        self._nodes = {}
        #self._edges = {}
        self._edges = nested_dict()
      
    @property
    def node(self):
        return self._nodes 
    
    @property
    def edge(self):
        return self._edges
    
    def nodes(self):
        return list(self._nodes.keys())
    
    def edges(self):
        return [(x,y) for x in self._edges.keys() for y in self._edges[x].keys()]
    
    def add_node(self, node, attr_dict=None):
        self._nodes[node] = dict() if not attr_dict else attr_dict
    
    def add_edge(self, node1, node2, attr_dict=None):
        attr_dict = dict() if not attr_dict else attr_dict 
        
        if node1 not in self._nodes:
            self.add_node(node1)
        if node2 not in self._nodes:
            self.add_node(node2)

        if node1 not in self._edges:
            self._edges[node1] = {}
        if node2 not in self._edges[node1]:
            self._edges[node1][node2] = {}
        self._edges[node1][node2].update(attr_dict)
            
        if node2 not in self._edges:
            self._edges[node2] = {}
        if node1 not in self._edges[node2]:
            self._edges[node2][node1] = {}
        self._edges[node2][node1].update(attr_dict)
                
    
    def add_nodes_from(self, node_list, attr_dict=None):
        attr_dict = dict() if not attr_dict else attr_dict
        for node in node_list:
            self.add_node(node, attr_dict)
    
    def add_edges_from(self, edge_list, attr_dict=None):
        attr_dict = dict() if not attr_dict else attr_dict
        for edge in edge_list:
            self.add_edge(edge[0], edge[1], attr_dict)
    
    def degree(self, node):
        return len(self._edges[node])
    
    def __getitem__(self, node):
        return self._edges[node]
    
    def __len__(self):
        return len(self._nodes)
    
    def neighbors(self, node):
        return list(self._edges[node].keys())
    
    def remove_node(self, node1):
        to_remove = list(self._edges[node1].keys())
        
        for node in to_remove:
            self.remove_edge(node1, node)
        self._nodes.pop(node1)
        self._edges.pop(node1)
    
    def remove_edge(self, node1, node2): 
        if((node1, node2) in self.edges() or (node2,node1) in self.edges()):
            self._edges[node1].pop(node2)
            self._edges[node2].pop(node1) 

    
    def remove_nodes_from(self, node_list):
        for node in node_list:
            self.remove_node(node)
    
    def remove_edges_from(self, edge_list):
        for edge in edge_list:
            self.remove_edge(edge[0], edge[1])